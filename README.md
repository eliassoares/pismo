# Teste Pismo
Este repositório contêm a minha solução para o teste da Pismo.

## Sobre a escolha da solução
Seguindo principalmente os dois primeiros critérios de avaliação, tentei deixar a solução simples e de fácil
manutenção. Há soluções mais robustas e complexas, mas escolhi a mais simples. Criei um pipeline, com Python e
PySpark onde os arquivos de eventos serão consolidados e salvos na pasta `events/trusted`, após serem processados os 
eventos serão movidos para a pasta `events/processed`. Abaixo segue um diagrama da arquitetura usada:

![arquitetura](architecture.png)

## Arquivos e pastas do projeto
- events/: contêm os arquivos e as "camadas" de dados do pipeline. Em  `events/origin` estão os arquivos enviados para 
teste, em `events/trusted` serão salvos os arquivos `parquet` finais e em `events/processed` estarão os arquivos após 
serem processados.
- src/: estão os scripts em si.
- test/: estão os testes unitários do projeto.
- .env: contêm as variáveis de ambientes usadas no container que executará o pipeline.
- .pylintrc: arquivo de configuração do [PyLint](https://pypi.org/project/pylint/).
- docker-compose.yml: arquivos que "configura" os containers para serem executados.
- Dockerfile: arquivo usado para construir a imagem docker.
- knowing-the-data.ipynb: arquivo usado para entender os dados, validar POC, e validar os dados após serem salvos 
após o término do pipeline.
- requirements.txt: arquivo com as dependências do projeto.

## Como executar
Todo o projeto foi desenvolvido usando [Docker](https://docs.docker.com/desktop/install/linux-install/) e 
[docker-compose](https://docs.docker.com/compose/install/linux/), portanto eles são requisitos para a execução do mesmo.
Para executar, siga os passos abaixo.

Construindo a imagem docker:
```bash
docker-compose build
```

Executando o pipeline:
```bash
docker-compose up pipeline
```

Como o pipeline move os arquivos de lugar, para executar o pipeline mais de uma vez, é necessário executar
os comandos abaixo:
```bash
mv events/processed/* events/origin
docker run -v $PWD:/home/jovyan/work/ --rm pismo_pipeline rm -r events/trusted/*
```

Executando os testes:
```bash
docker run -v $PWD:/home/jovyan/work/ -e PYTHONPATH="${PYTONPATH}:/home/joyvan/work/src" --rm pismo_pipeline pytest test -v
```
 
Executando o lint:
```bash
docker run -v $PWD:/home/jovyan/work/ -e PYTHONPATH="${PYTONPATH}:/home/joyvan/work/src" --rm pismo_pipeline pylint src
```

## Uso do notebook
Criei um notebook [knowing-the-data.ipynb](knowing-the-data.ipynb) para entender os dados os eventos, criar protótipos
da solução final e validar os dados já salvos pelo pipeline. Caso queiram executá-lo, há um container que permite a 
execução do mesmo, basta executar o comando abaixo:
```bash
docker-compose up notebook
```

Valeu, foi divertido :D

![thanks](https://media.giphy.com/media/U7Jrpeu6q78XSZZAPT/giphy-downsized.gif)
