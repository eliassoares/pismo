"""
    This module implements the events processor. Will load the file events and
    save then in final format files.
"""

import os
from typing import List

from pyspark.sql import SparkSession, DataFrame
from pyspark.sql.functions import col, regexp_replace, substring, concat, lit

import src.common


class EventProcessor:
    """
        This class implements the events processor.
    """

    def __init__(self, spark_session: SparkSession):
        self.spark = spark_session
        self.trusted_path = os.getenv('TRUSTED_FILES_PATH')
        self.origin_files_path = os.getenv('ORIGIN_FILES_PATH')
        self.processed_files_path = os.getenv('PROCESSED_FILES_PATH')

    def _get_dataframe_by_files(self, files: List[str]) -> DataFrame:
        """
        Given a list of files, will parses the files and returns a DataFrame containing the data.
        :param files: list of event files.
        :return: DataFrame with data.
        """
        return (
            self.spark.read.json(
                self.spark.sparkContext.wholeTextFiles(
                    ','.join(files)
                ).values().flatMap(lambda x: x.replace("}{", "}#,#{").split("#,#"))
            )
        )

    def _get_files_to_read(self) -> list:
        """
        Returns a list of event files to read, based on ORIGIN_FILES_PATH environment variable.
        :return: list of event files to read.
        """
        return [f'{self.origin_files_path}/{f}' for f in os.listdir(self.origin_files_path)]

    def _get_dataframe_with_partition_column(self, dataframe: DataFrame) -> DataFrame:
        """
        Given a Spark DataFrame, will return a new DataFrame with the partition column, concatenating domain,
        event_type and part of timestamp columns.
        :param dataframe: dataframe to create a new one
        :return: DataFrame with partition column.
        """
        # We will create a partition column that we will use as the file path when saving,
        # eg: transaction/creation/2021/01/01
        return (
            dataframe.withColumn('partition', concat(col('domain'), lit('/'), col('event_type'), lit('/')))
            .withColumn('date', substring(col('timestamp'), 0, 10))
            .withColumn('date', regexp_replace(col('date'), '-', '/'))
            .withColumn('partition', concat(col('partition'), col('date'))).drop('date')
        )

    def _save_events(self, dataframe: DataFrame):
        """
        Given a dataframe with the events will made some transformations and save the events with the appropriate
        partitions.
        :param dataframe: dataframe to save the events.
        """
        # Transforming the data fields into columns:
        dataframe = dataframe.select('domain', 'event_id', 'event_type', 'timestamp', 'data.*')
        dataframe = self._get_dataframe_with_partition_column(dataframe)

        partitions = [r.partition for r in dataframe.select('partition').distinct().collect()]
        for partition in partitions:
            df_to_save = dataframe.filter(col('partition') == partition)
            # The dataframe contains the schema of all events types, we will force it to save each
            # event with its own schema:
            df_to_save = src.common.drop_all_null_columns(df_to_save)

            df_to_save.drop('partition').write.mode('append').parquet(
                f'{self.trusted_path}/{partition}'
            )

    def run(self):
        """
        This function it is responsible for run the pipeline.
        """
        files_chunk_size = int(os.getenv('FILES_BATCH_SIZE'))
        all_files = self._get_files_to_read()
        files_batch = list(src.common.get_splitted_array(all_files, files_chunk_size))

        for files_list in files_batch:
            # There are two kinds of timestamp '2021-01-01T15:19:57' and '2021-01-01 15:19:57'
            # I will "normalize" and put all in same format. This issue can influences in
            # deduplication of repeated events.
            dataframe = self._get_dataframe_by_files(files_list).withColumn(
                'timestamp', regexp_replace(col('timestamp'), ' ', 'T')
            )
            # Removing duplicated events and keeping the more recent ones:
            dataframe = src.common.get_deduplicated_dataframe(dataframe, 'event_id', 'timestamp')
            self._save_events(dataframe)
            src.common.move_files(files_list, self.processed_files_path)


if __name__ == '__main__':
    spark = SparkSession.builder.appName('event-processor').getOrCreate()
    event_processor = EventProcessor(spark)

    event_processor.run()
