"""
    This files contains functions that can be imported into another modules.
"""

import os

from pyspark.sql import DataFrame
from pyspark.sql.functions import col, row_number, count, when
from pyspark.sql.window import Window


def get_splitted_array(array: list, chunk_size: int) -> list:
    """
    Given an array and a chunk size, will return the array splitted in chunks.
    Eg: array = [1, 2, 3, 4], chunk_size = 2, returns [[1, 2], [3, 4]]
    :param array: array to be splitted
    :param chunk_size: chunk size
    :return: new array of splitted array
    """
    if not chunk_size:
        return []

    for i in range(0, len(array), chunk_size):
        yield array[i:i + chunk_size]


def get_deduplicated_dataframe(dataframe: DataFrame, partition_col: str, order_by_col) -> DataFrame:
    """
    Given a dataframe, a partition column and a column to sort the dataframe, will return a DataFrame without
    duplicated data, using the partition_col to identify the duplicated and the order_by_col to get only the more recent
    :param dataframe: dataframe to deduplicate.
    :param partition_col: column to use in deduplication.
    :param order_by_col: column to sort.
    :return: dataframe with duplicated data.
    """
    return (
        dataframe.withColumn('row_number', row_number().over(
            Window.partitionBy(dataframe[partition_col]).orderBy(dataframe[order_by_col].desc())))
        .filter(col('row_number') == 1).drop('row_number')
    )


def move_files(files_list: list, destination: str):
    """
    Given a list of files and a destination, move the files to the destination.
    :param files_list: list of files to move
    :param destination: destination directory
    """
    if not files_list or not destination:
        return

    for file in files_list:
        file_name = file.split('/')[-1]
        os.replace(file, f'{destination}/{file_name}')


def drop_all_null_columns(dataframe: DataFrame) -> DataFrame:
    """
    Given a dataframe, will returns a DataFrame without columns that have only null values.
    :param dataframe: dataframe to drop columns without data.
    :return: dataframe that has only not null columns.
    """
    total = dataframe.count()
    # columns_null_count is a dictionary that maps column names to total of null values.
    # eg: {'domain': 0, 'event_id': 0, 'event_type': 0, 'timestamp': 0, 'addresses': 9922, 'amount': 19880, ...}
    columns_null_count = dataframe.select(
        [
            count(when(col(c).isNull(), c)).alias(c) for c in dataframe.columns
        ]
    ).collect()[0].asDict()

    # Filtering column that all values are null:
    col_to_drop = [column for column, null_count in columns_null_count.items() if null_count == total]
    return dataframe.drop(*col_to_drop)
