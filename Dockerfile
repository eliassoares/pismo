FROM jupyter/pyspark-notebook

WORKDIR /home/jovyan/work

USER root

COPY requirements.txt /home/jovyan/work

RUN pip install -r requirements.txt

RUN rm /home/jovyan/work/requirements.txt
