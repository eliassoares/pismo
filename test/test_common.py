# This import is used to find spark on tests
import findspark
findspark.init()

from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, IntegerType

from pyspark_test import assert_pyspark_df_equal
from pytest_mock.plugin import MockerFixture

from src.common import (
    get_splitted_array, get_deduplicated_dataframe, move_files, drop_all_null_columns
)

spark = SparkSession.builder.appName('tests').getOrCreate()


def test_drop_all_null_columns_when_df_has_one_column_null():
    data = [(1, 'Java', None), (2, 'Python', None), (3, 'Go', None)]
    dataframe = spark.sparkContext.parallelize(data).toDF(StructType(
        [
            StructField('id', IntegerType(), True),
            StructField('language', StringType(), True),
            StructField('started_at', StringType(), True)
        ]
    ))
    expected_dataframe = spark.sparkContext.parallelize([
        (1, 'Java'), (2, 'Python'), (3, 'Go')
    ]).toDF(StructType(
        [
            StructField('id', IntegerType(), True),
            StructField('language', StringType(), True),
        ]
    ))

    assert_pyspark_df_equal(
        expected_dataframe, drop_all_null_columns(dataframe)
    )


def test_drop_all_null_columns_when_df_columns_has_least_one_not_null_value():
    data = [(1, 'Java', None), (2, 'Python', None), (3, 'Go', '2017-03-25')]
    dataframe = spark.sparkContext.parallelize(data).toDF(['id', 'language', 'started_at'])

    assert_pyspark_df_equal(
        dataframe, drop_all_null_columns(dataframe)
    )


def test_drop_all_null_columns_when_df_columns_has_not_null_values():
    data = [(1, 'Java', '2013-08-25'), (2, 'Python', '2014-06-14'), (3, 'Go', '2017-03-25')]
    dataframe = spark.sparkContext.parallelize(data).toDF(['id', 'language', 'started_at'])

    assert_pyspark_df_equal(
        dataframe, drop_all_null_columns(dataframe)
    )


def test_move_files_when_destination_is_empty(mocker: MockerFixture):
    files = ['files1']
    destination = ''
    mocked_replace = mocker.patch('os.replace')
    move_files(files, destination)
    mocked_replace.assert_not_called()


def test_move_files_when_with_valid_parameters(mocker: MockerFixture):
    files = [
        './origin/events-1f7df57a-e1c9-4b76-b795-962de9cc292e.json',
        './origin/events-88d5d460-d3ab-4058-9abd-fd07e033a337.json',
    ]
    destination = './processed'
    expected_calls = [
        mocker.call(
            './origin/events-1f7df57a-e1c9-4b76-b795-962de9cc292e.json',
            './processed/events-1f7df57a-e1c9-4b76-b795-962de9cc292e.json'
        ),
        mocker.call(
            './origin/events-88d5d460-d3ab-4058-9abd-fd07e033a337.json',
            './processed/events-88d5d460-d3ab-4058-9abd-fd07e033a337.json'
        ),
    ]

    mocked_replace = mocker.patch('os.replace')
    move_files(files, destination)
    mocked_replace.assert_has_calls(expected_calls)


def test_move_files_when_files_list_is_empty(mocker: MockerFixture):
    files = []
    destination = '/destination'
    mocked_replace = mocker.patch('os.replace')
    move_files(files, destination)
    mocked_replace.assert_not_called()


def test_get_deduplicated_dataframe_when_does_have_duplicated_data():
    duplicated_data = [
        (1, 'Java', '2013-08-25'), (2, 'Python', '2014-06-14'), (3, 'Go', '2017-03-25'), (3, 'Go', '2017-03-26')
    ]
    duplicated_dataframe = spark.sparkContext.parallelize(duplicated_data).toDF(['id', 'language', 'started_at'])
    expected_dataframe = spark.sparkContext.parallelize(
        [(1, 'Java', '2013-08-25'), (2, 'Python', '2014-06-14'), (3, 'Go', '2017-03-26')]
    ).toDF(['id', 'language', 'started_at'])

    assert_pyspark_df_equal(get_deduplicated_dataframe(duplicated_dataframe, 'id', 'started_at'), expected_dataframe)


def test_get_deduplicated_dataframe_when_does_not_have_duplicated_data():
    data = [(1, 'Java', '2013-08-25'), (2, 'Python', '2014-06-14'), (3, 'Go', '2017-03-25')]
    dataframe = spark.sparkContext.parallelize(data).toDF(['id', 'language', 'started_at'])

    assert_pyspark_df_equal(get_deduplicated_dataframe(dataframe, 'id', 'started_at'), dataframe)


def test_get_splitted_array_when_array_is_empty():
    assert list(get_splitted_array([], 1)) == []


def test_get_splitted_array_when_chunk_size_is_zero():
    assert list(get_splitted_array([1, 2], 0)) == []


def test_get_splitted_array_when_chunk_size_is_smaller_than_array_len():
    assert list(get_splitted_array([1, 2, 3], 2)) == [[1, 2], [3]]


def test_get_splitted_array_when_chunk_size_is_bigger_than_array_len():
    assert list(get_splitted_array([1, 2, 3], 4)) == [[1, 2, 3]]
