# This import is used to find spark on tests
import findspark
findspark.init()

import os
import shutil
from unittest.mock import patch, Mock

from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, LongType
from pyspark.sql.utils import IllegalArgumentException, Py4JJavaError
from pyspark_test import assert_pyspark_df_equal
from pytest import raises
from pytest_mock.plugin import MockerFixture

from src.event_processor import EventProcessor

spark = SparkSession.builder.appName('tests').getOrCreate()
environment_variables = {
    'TRUSTED_FILES_PATH': './test/data/trusted',
    'ORIGIN_FILES_PATH': './test/data/origin',
    'PROCESSED_FILES_PATH': './test/data/processed',
    'FILES_BATCH_SIZE': '5'
}
dataframe_fixture = spark.sparkContext.parallelize([
        ('transaction', 'creation', '2013-08-25T00:00:00', {'status': 'ok'}, '1'),
        ('account', 'creation', '2014-06-14T00:00:00', {'status': 'ok'}, '2'),
        ('transaction', 'creation', '2017-03-25T00:00:00', {'status': 'ok'}, '3')
    ]).toDF(
    StructType([
        StructField('domain', StringType(), True),
        StructField('event_type', StringType(), True),
        StructField('timestamp', StringType(), True),
        StructField('data', StructType([
            StructField('status', StringType(), True),
        ])),
        StructField('event_id', StringType(), True),
    ])
)


@patch.dict(os.environ, environment_variables)
def test_run(mocker: MockerFixture):
    move_files_mock = mocker.patch('src.common.move_files')
    get_deduplicated_dataframe_mock = mocker.patch('src.common.get_deduplicated_dataframe')
    event_processor = EventProcessor(spark)
    event_processor._save_events = Mock()

    expected_file_list = ['./test/data/origin/events-1f7df57a-e1c9-4b76-b795-962de9cc292e-sample.json']
    expected_processed_destination = './test/data/processed'

    event_processor.run()

    event_processor._save_events.assert_called_once()
    get_deduplicated_dataframe_mock.assert_called_once()
    move_files_mock.assert_called_once_with(expected_file_list, expected_processed_destination)


@patch.dict(os.environ, environment_variables)
def test_save_events():
    event_processor = EventProcessor(spark)
    event_processor._save_events(dataframe_fixture)

    # Checking if the events have been saved:
    assert list(filter(
        lambda file: file.endswith('.parquet'),
        os.listdir('test/data/trusted/transaction/creation/2017/03/25')
    )) != []
    assert list(filter(
        lambda file: file.endswith('.parquet'),
        os.listdir('test/data/trusted/account/creation/2014/06/14')
    )) != []
    assert list(filter(
        lambda file: file.endswith('.parquet'),
        os.listdir('test/data/trusted/transaction/creation/2013/08/25')
    )) != []

    # Cleaning the saved events:
    shutil.rmtree('/home/jovyan/work/test/data/trusted/transaction')
    shutil.rmtree('/home/jovyan/work/test/data/trusted/account')


def test_get_dataframe_with_partition_column_when_dataframe_has_no_data():
    event_processor = EventProcessor(spark)
    dataframe = spark.sparkContext.parallelize([]).toDF(StructType(
        [
            StructField('domain', StringType(), True),
            StructField('event_type', StringType(), True),
            StructField('timestamp', StringType(), True)
        ]
    ))
    expected_dataframe = spark.sparkContext.parallelize([]).toDF(StructType(
        [
            StructField('domain', StringType(), True),
            StructField('event_type', StringType(), True),
            StructField('timestamp', StringType(), True),
            StructField('partition', StringType(), True)
        ]
    ))
    result = event_processor._get_dataframe_with_partition_column(dataframe)
    assert_pyspark_df_equal(result, expected_dataframe)


def test_get_dataframe_with_partition_column_when_dataframe_has_data():
    event_processor = EventProcessor(spark)
    data = [
        ('transaction', 'creation', '2013-08-25T00:00:00'),
        ('account', 'creation', '2014-06-14T00:00:00'),
        ('transaction', 'creation', '2017-03-25T00:00:00')
    ]
    columns = ['domain', 'event_type', 'timestamp']
    dataframe = spark.sparkContext.parallelize(data).toDF(columns)
    expected_columns = ['domain', 'event_type', 'timestamp', 'partition']
    expected_partitions = [
        'transaction/creation/2013/08/25',
        'account/creation/2014/06/14',
        'transaction/creation/2017/03/25',
    ]

    result = event_processor._get_dataframe_with_partition_column(dataframe)
    assert result.columns == expected_columns
    assert [r.partition for r in result.select('partition').collect()] == expected_partitions


@patch.dict(os.environ, environment_variables)
def test_get_files_to_read_when_there_is_no_file(mocker: MockerFixture):
    mocker.patch('os.listdir', return_value=[])
    event_processor = EventProcessor(spark)
    expected_files = []

    assert event_processor._get_files_to_read() == expected_files


@patch.dict(os.environ, environment_variables)
def test_get_files_to_read_when_there_is_file():
    event_processor = EventProcessor(spark)
    expected_files = [
        './test/data/origin/events-1f7df57a-e1c9-4b76-b795-962de9cc292e-sample.json'
    ]

    assert event_processor._get_files_to_read() == expected_files


def test_get_dataframe_by_files_when_the_files_exist():
    event_processor = EventProcessor(spark)
    expected_count = 3
    expected_schema = StructType([
        StructField('data', StructType([
            StructField('amount', StringType(), True),
            StructField('id', LongType(), True),
            StructField('new_status', StringType(), True),
            StructField('old_status', StringType(), True),
            StructField('reason', StringType(), True),
            StructField('recipient', StructType([
                StructField('carrier', StringType(), True),
                StructField('msisdn', StringType(), True),
            ]), True),
            StructField('type', StringType(), True),
        ])),
        StructField('domain', StringType(), True),
        StructField('event_id', StringType(), True),
        StructField('event_type', StringType(), True),
        StructField('timestamp', StringType(), True),
    ])

    dataframe = event_processor._get_dataframe_by_files([
        '/home/jovyan/work/test/data/origin/events-1f7df57a-e1c9-4b76-b795-962de9cc292e-sample.json'
    ])
    assert dataframe.count() == expected_count
    assert dataframe.schema == expected_schema


def test_get_dataframe_by_files_when_the_files_not_exist():
    event_processor = EventProcessor(spark)

    with raises(Py4JJavaError):
        event_processor._get_dataframe_by_files(['not-today.json'])


def test_get_dataframe_by_files_when_list_files_is_empty():
    event_processor = EventProcessor(spark)
    with raises(IllegalArgumentException):
        event_processor._get_dataframe_by_files([])


@patch.dict(os.environ, environment_variables)
def test_init():
    event_processor = EventProcessor(spark)

    assert event_processor.spark == spark
    assert event_processor.trusted_path == './test/data/trusted'
    assert event_processor.origin_files_path == './test/data/origin'
    assert event_processor.processed_files_path == './test/data/processed'
